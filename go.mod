module gitlab.com/aswinkarthik93/karthik

go 1.13

require (
	github.com/antchfx/htmlquery v1.2.2
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/stretchr/testify v1.4.0
	go.etcd.io/bbolt v1.3.3
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
)
