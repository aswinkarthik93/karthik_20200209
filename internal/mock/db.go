package mock

import (
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/db"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"sync"
)

var _ db.DB = &DB{}

type DB struct {
	AddRecipeFunc  func(domain.Recipe, domain.Link) error
	ListLinksFunc  func() ([]string, error)
	ListRecipeFunc func() ([]domain.Recipe, error)
	Lock           *sync.Mutex
	Recipes        []domain.Recipe
	Links          []domain.Link
}

func DefaultMockDB() *DB {
	mockDB := &DB{
		Lock:    &sync.Mutex{},
		Recipes: make([]domain.Recipe, 0),
		Links:   make([]domain.Link, 0),
	}

	mockDB.AddRecipeFunc = addRecipe(mockDB)
	mockDB.ListLinksFunc = listLinks(mockDB)
	mockDB.ListRecipeFunc = listRecipes(mockDB)

	return mockDB
}

func (m DB) AddRecipeWithLink(recipe domain.Recipe, link domain.Link) error {
	return m.AddRecipeFunc(recipe, link)
}

func (m DB) ListRecipes() ([]domain.Recipe, error) {
	return m.ListRecipeFunc()
}

func (m DB) ListLinks() ([]string, error) {
	return m.ListLinksFunc()
}

func addRecipe(m *DB) func(domain.Recipe, domain.Link) error {
	return func(recipe domain.Recipe, link domain.Link) error {
		for _, r := range m.Recipes {
			if r.Name == recipe.Name {
				return fmt.Errorf("recipe already exists")
			}
		}

		for _, l := range m.Links {
			if l.URL == link.URL {
				return fmt.Errorf("link already exists")
			}
		}

		m.Lock.Lock()
		m.Recipes = append(m.Recipes, recipe)
		m.Links = append(m.Links, link)
		m.Lock.Unlock()

		return nil
	}
}

func listLinks(m *DB) func() ([]string, error) {
	return func() ([]string, error) {
		links := make([]string, 0, len(m.Links))
		for _, link := range m.Links {
			links = append(links, link.URL)
		}
		return links, nil
	}
}

func listRecipes(m *DB) func() ([]domain.Recipe, error) {
	return func() ([]domain.Recipe, error) {
		return m.Recipes, nil
	}
}
