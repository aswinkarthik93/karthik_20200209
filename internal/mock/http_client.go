package mock

import (
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/http"
	"sync"
)

var _ http.HTMLClient = &MockHTMLClient{}

func NewMockClient() *MockHTMLClient {
	return &MockHTMLClient{
		mockResponses: make(map[string]string),
		responseCount: make(map[string]int),
		errResponses:  make(map[string]error),
		mutex:         &sync.Mutex{},
	}
}

type MockHTMLClient struct {
	mockResponses map[string]string
	responseCount map[string]int
	errResponses  map[string]error
	mutex         *sync.Mutex
}

func (m *MockHTMLClient) Get(url string) (string, error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	if err, exists := m.errResponses[url]; exists {
		count := m.responseCount[url] + 1
		m.responseCount[url] = count

		return "", err
	}

	if body, exists := m.mockResponses[url]; exists {
		count := m.responseCount[url] + 1
		m.responseCount[url] = count

		return body, nil
	}
	return "", fmt.Errorf("no such mock")
}

func (m *MockHTMLClient) MockResponse(url, body string) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	if _, exists := m.mockResponses[url]; exists {
		return fmt.Errorf("mock already exists")
	}

	m.mockResponses[url] = body

	return nil
}

func (m *MockHTMLClient) MockError(url string, err error) error {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	if _, exists := m.errResponses[url]; exists {
		return fmt.Errorf("mock already exists")
	}

	m.errResponses[url] = err

	return nil
}

func (m *MockHTMLClient) GetResponseCount(url string) (int, error) {
	_, responseExists := m.mockResponses[url]
	_, errorExists := m.errResponses[url]
	if !errorExists && !responseExists {
		return 0, fmt.Errorf("no such mocks")
	}

	return m.responseCount[url], nil
}
