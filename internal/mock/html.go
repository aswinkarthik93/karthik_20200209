package mock

import (
	"strings"
	"text/template"
)

func LinksHTML(links []string) (string, error) {
	templateStr := `
<!doctype html>
<html lang="en">
<head>
    
    <title>Posts - http://some-url/</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="well well-sm">
                <strong>Blog Post Archive</strong>
                <ul class="list-unstyled">
{{- range . }}
		<li>
        <h5><a href="{{ . }}">Item Name</a><br>
        <small>posted on February 7, 2020</small></h5>
		</li>
{{- end }}
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
`
	tmpl, err := template.New("").Parse(templateStr)
	if err != nil {
		return "", err
	}

	out := strings.Builder{}
	if err := tmpl.Execute(&out, links); err != nil {
		return "", err
	}
	return out.String(), nil
}

func RecipeHTML(recipeName string) string {
	return strings.ReplaceAll(`
<!doctype html>
<html lang="en">

<head>
    <title>:recipeName: - http://some-url/</title>
    <link rel="canonical" href="http://some-url/post/green-spaghetti/">
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-9">
            	<div class="well well-sm">
                    <h3>:recipeName:<br> <small></small></h3>
                    <hr>
                    <h3 id="description">Description</h3>
<p>Start to finish this whole recipe takes about 10 minutes, but it&rsquo;s pretty rapid fire once you get stated. Have plates and an appetite standing by.</p>
<h3 id="ingredients">Ingredients</h3>
<ul>
<li>dried spaghetti</li>
<li>giant bunch of dark greens (see notes)</li>
<li>4 cloves of garlic</li>
<li>1/4 cup parmesan cheese</li>
<li>1.4 cup ricotta or queso fresco</li>
</ul>
<h3 id="steps">Steps</h3>
<ul>
<li>Bring water to a boil and add spaghetti, greens, and whole garlic cloves</li>
<li>Wilt the greens for a few minutes</li>
<li>Use tongs to move greens and garlic to a blender, add fresh-grated parmesan, pulse for a few mins, add salt and pepper to taste</li>
<li>Drain pasta, reserving a cup or so of the water</li>
<li>Toss pasta in sauce, adding a splash of water to make everything happy</li>
<li>Plate and top with freshly crumbled cheese and more cracked black pepper</li>
</ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well well-sm"> 
                <h4>February 7, 2020<br>
                <small>131 words</small></h4>
                <hr>
                <strong>Categories</strong>
                <ul class="list-unstyled">
                
                    <li><a href="http://some-url/categories/recipe">recipe</a></li>
                
                    <li><a href="http://some-url/categories/fast">fast</a></li>
                
                </ul>
                <hr>
                <strong>Tags</strong><br>
                <a class="label label-default" href="http://some-url/tags/mains">mains</a> <a class="label label-default" href="http://some-url/tags/pasta">pasta</a>
            	</div>
			</div>
		</div>
	</div>
</body>
</html>`, ":recipeName:", recipeName)
}
