package mock

import (
	"gitlab.com/aswinkarthik93/karthik/domain"
)

func BuildRecipeWithName(recipeName string) domain.Recipe {
	return domain.Recipe{
		Name:        recipeName,
		Description: "Start to finish this whole recipe takes about 10 minutes, but it’s pretty rapid fire once you get stated. Have plates and an appetite standing by.",
		Ingredients: []string{
			"dried spaghetti",
			"giant bunch of dark greens (see notes)",
			"4 cloves of garlic",
			"1/4 cup parmesan cheese",
			"1.4 cup ricotta or queso fresco",
		},
		PreparationSteps: []string{
			"Bring water to a boil and add spaghetti, greens, and whole garlic cloves",
			"Wilt the greens for a few minutes",
			"Use tongs to move greens and garlic to a blender, add fresh-grated parmesan, pulse for a few mins, add salt and pepper to taste",
			"Drain pasta, reserving a cup or so of the water",
			"Toss pasta in sauce, adding a splash of water to make everything happy",
			"Plate and top with freshly crumbled cheese and more cracked black pepper",
		},
		Categories: []string{"recipe", "fast"},
		Tags:       []string{"mains", "pasta"},
	}
}
