package mock

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMockHTMLClient(t *testing.T) {

	t.Run("should handle no mocks", func(t *testing.T) {
		t.Parallel()
		mockClient := NewMockClient()
		body, err := mockClient.Get("http://google.com")
		assert.EqualError(t, err, "no such mock")
		assert.Equal(t, "", body)
	})

	t.Run("should handle duplicate mocks", func(t *testing.T) {
		t.Parallel()
		mockClient := NewMockClient()
		err := mockClient.MockResponse("http://google.com", "welcome to google")
		assert.NoError(t, err)

		err = mockClient.MockResponse("http://google.com", "welcome to google second time")
		assert.EqualError(t, err, "mock already exists")
	})

	t.Run("should mock url with body", func(t *testing.T) {
		t.Parallel()
		mockClient := NewMockClient()
		expectedBody := "welcome to google"
		err := mockClient.MockResponse("http://google.com", expectedBody)
		assert.NoError(t, err)

		body, err := mockClient.Get("http://google.com")

		assert.NoError(t, err)
		assert.Equal(t, expectedBody, body)
	})

	t.Run("should count mocks called", func(t *testing.T) {
		t.Parallel()
		mockClient := NewMockClient()
		err := mockClient.MockResponse("http://google.com", "body")
		assert.NoError(t, err)
		err = mockClient.MockResponse("http://gmail.com", "body")
		assert.NoError(t, err)

		_, err = mockClient.Get("http://google.com")
		assert.NoError(t, err)
		_, err = mockClient.Get("http://google.com")
		assert.NoError(t, err)

		count, err := mockClient.GetResponseCount("http://google.com")
		assert.NoError(t, err)
		assert.Equal(t, 2, count)

		_, err = mockClient.GetResponseCount("http://fb.com")
		assert.EqualError(t, err, "no such mocks")

		count, err = mockClient.GetResponseCount("http://gmail.com")
		assert.NoError(t, err)
		assert.Equal(t, 0, count)
	})

	t.Run("should mock errors", func(t *testing.T) {
		t.Parallel()
		mockClient := NewMockClient()
		err := mockClient.MockError("http://google.com", fmt.Errorf("not available"))
		assert.NoError(t, err)

		_, err = mockClient.Get("http://google.com")
		assert.EqualError(t, err, "not available")
	})
}
