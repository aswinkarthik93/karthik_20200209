package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/db"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/http"
	"gitlab.com/aswinkarthik93/karthik/scraper"
	"gitlab.com/aswinkarthik93/karthik/utils"
	"os"
	"strconv"
	"strings"
)

var (
	url      string
	dbPath   string
	sanitize bool
)

func init() {
	flag.StringVar(&url, "url", "", "URL to scrape from. Env: ${RECIPE_CRAWLER_URL}")
	flag.StringVar(&dbPath, "db-path", "./data.db", "File path to save the DB. Env: ${RECIPE_CRAWLER_DB_PATH}")
	flag.BoolVar(&sanitize, "sanitize-url", true, "Append /post/ if it is missing in URL. Env: ${RECIPE_CRAWLER_SANITIZE_URL}")
}

func usage() {
	_, _ = fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func parseFlags() {
	flag.Parse()
	if val := os.Getenv("RECIPE_CRAWLER_URL"); val != "" {
		url = val
	}
	if val := os.Getenv("RECIPE_CRAWLER_DB_PATH"); val != "" {
		dbPath = val
	}
	if parseBool, err := strconv.ParseBool(os.Getenv("RECIPE_CRAWLER_SANITIZE_URL")); err == nil {
		sanitize = parseBool
	}
}

func main() {
	parseFlags()

	if url == "" {
		usage()
		fail(fmt.Errorf("-url is mandatory"))
	}

	url = sanitizeURL(url)

	if dbPath == "" {
		usage()
		fail(fmt.Errorf("-db-path is mandatory"))
	}

	store, err := db.NewBoltDB(dbPath, nil)
	if err != nil {
		fail(err)
	}
	htmlClient := http.NewHTTPClient()
	recipes, err := run(store, htmlClient, url)
	if err != nil {
		fail(err)
	}
	content, _ := json.MarshalIndent(recipes, "", "  ")
	_, _ = fmt.Fprintln(os.Stdout, string(content))

}

func sanitizeURL(urlToSanitize string) string {
	if !strings.HasSuffix(urlToSanitize, "/post") && !strings.HasSuffix(urlToSanitize, "/post/") {
		if strings.HasSuffix(urlToSanitize, "/") {
			urlToSanitize = fmt.Sprintf("%s%s", urlToSanitize, "post")
		} else {
			urlToSanitize = fmt.Sprintf("%s/%s", urlToSanitize, "post")
		}
	}

	return urlToSanitize
}

func fail(err error) {
	_, _ = fmt.Fprintf(os.Stderr, "recipe-crawler: command failed with reason %s\n", err.Error())
	os.Exit(1)
}

func run(db db.DB, client http.HTMLClient, url string) ([]domain.Recipe, error) {
	links, err := db.ListLinks()
	if err != nil {
		return nil, fmt.Errorf("error retrieving links: %v", err)
	}
	visitedLinks := utils.NewStringSet(links)

	linksScraper := scraper.NewLinksScraper(url, client, visitedLinks)

	go linksScraper.Run()

	recipeChan := linksScraper.RecipeChan()
	errChan := linksScraper.ErrChan()

	for done := false; !done; {
		select {
		case parsedRecipe, open := <-recipeChan:
			if !open {
				done = true
				break
			}

			recipe := parsedRecipe.Recipe
			link := parsedRecipe.Link
			if err := db.AddRecipeWithLink(recipe, domain.Link{
				URL:        link,
				RecipeName: recipe.Name,
			}); err != nil {
				return nil, fmt.Errorf("saving %s failed: %v", recipe.Name, err)
			}
		case err := <-errChan:
			return nil, err
		}
	}

	recipes, err := db.ListRecipes()
	if err != nil {
		return nil, fmt.Errorf("error retrieving recipes: %v", err)
	}

	return recipes, nil
}
