package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/internal/mock"
	"testing"
)

func TestRun(t *testing.T) {
	t.Run("should run successfully and fetch all recipes in DB", func(t *testing.T) {
		db := mock.DefaultMockDB()
		client := mock.NewMockClient()
		urlToFetch := "http://some-url"

		recipeLink1 := "http://some-url/recipe-1"
		recipeLink2 := "http://some-url/recipe-2"

		rootLinkContent, err := mock.LinksHTML([]string{recipeLink1, recipeLink2})
		assert.NoError(t, err)
		assert.NoError(t, client.MockResponse(urlToFetch, rootLinkContent))
		assert.NoError(t, client.MockResponse(recipeLink1, mock.RecipeHTML("Pizza")))
		assert.NoError(t, client.MockResponse(recipeLink2, mock.RecipeHTML("Pasta")))

		recipes, err := run(db, client, urlToFetch)

		expectedRecipes := []domain.Recipe{
			mock.BuildRecipeWithName("Pasta"),
			mock.BuildRecipeWithName("Pizza"),
		}

		assert.NoError(t, err)
		assert.Equal(t, expectedRecipes, recipes)
		assert.ElementsMatch(t, expectedRecipes, db.Recipes)

		expectedLinks := []domain.Link{
			{URL: "http://some-url/recipe-2", RecipeName: "Pasta"},
			{URL: "http://some-url/recipe-1", RecipeName: "Pizza"},
		}
		assert.ElementsMatch(t, expectedLinks, db.Links)
	})

	t.Run("should skip URL if already fetched", func(t *testing.T) {
		db := mock.DefaultMockDB()
		client := mock.NewMockClient()
		urlToFetch := "http://some-url"

		recipeLink1 := "http://some-url/recipe-1"
		recipeLink2 := "http://some-url/recipe-2"

		pasta := mock.BuildRecipeWithName("Pasta")
		pizza := mock.BuildRecipeWithName("Pizza")
		pastaLink := domain.Link{URL: recipeLink2, RecipeName: "Pasta"}

		db.Links = append(db.Links, pastaLink)
		db.Recipes = append(db.Recipes, pasta)

		rootLinkContent, err := mock.LinksHTML([]string{recipeLink1, recipeLink2})
		assert.NoError(t, err)
		assert.NoError(t, client.MockResponse(urlToFetch, rootLinkContent))
		assert.NoError(t, client.MockResponse(recipeLink1, mock.RecipeHTML("Pizza")))
		assert.NoError(t, client.MockError(recipeLink2, fmt.Errorf("this should not be called")))

		recipes, err := run(db, client, urlToFetch)

		expectedRecipes := []domain.Recipe{
			pasta,
			pizza,
		}

		assert.NoError(t, err)
		assert.Equal(t, expectedRecipes, recipes)

		{
			count, err := client.GetResponseCount(recipeLink1)
			assert.NoError(t, err)
			assert.Equal(t, 1, count)
		}

		{
			count, err := client.GetResponseCount(recipeLink2)
			assert.NoError(t, err)
			assert.Equal(t, 0, count)
		}
	})

	t.Run("should return error if any fetch fails", func(t *testing.T) {
		db := mock.DefaultMockDB()
		client := mock.NewMockClient()
		urlToFetch := "http://some-url"

		assert.NoError(t, client.MockError(urlToFetch, fmt.Errorf("error occurred")))

		_, err := run(db, client, urlToFetch)
		assert.EqualError(t, err, "error fetching contents: error occurred")
	})
}
