# KARTHIK_20200209

This is golang project to save cooking recipes.

## Components

It scrapes from a URL all the recipes and stores it in a database. [BoltDB](https://github.com/etcd-io/bbolt) is used as the persistence layer.

## Usage

```bash
$ recipe-crawler
Usage of ./recipe-crawler:
  -db-path string
        File path to save the DB. Env: ${RECIPE_CRAWLER_DB_PATH} (default "./data.db")
  -sanitize-url
        Append /post/ if it is missing in URL. Env: ${RECIPE_CRAWLER_SANITIZE_URL} (default true)
  -url string
        URL to scrape from. Env: ${RECIPE_CRAWLER_URL}
```

## Run

To run using docker,

```bash
## Clone the repo
$ git clone https://gitlab.com/aswinkarthik93/karthik_20200209.git

## Change directory
$ cd karthik_20200209

## This would crawl all URLs and store it in boltDB stored in ./data/data.db
$ docker-compose up

## Running again will skip fetching all URLs as they are already stored in DB
```

To run directly,

```bash
## Build code
$ go build -o recipe-crawler ./main.go

## Run
$ ./recipe-crawler -url ${ROOT_URL_TO_CRAWL} -db-path ${PATH_TO_DB}
```

## Configurations

There are 2 configurations that can be passed

| Flag         | Usage                                  | EnvVar                      | Default   | Required |
|--------------|----------------------------------------|-----------------------------|-----------|----------|
| url          | URL to scrape from                     | RECIPE_CRAWLER_URL          | -         | Yes      |
| db-path      | File path to save the DB.              | RECIPE_CRAWLER_DB_PATH      | ./data.db | Yes      |
| sanitize-url | Append /post/ if it is missing in URL. | RECIPE_CRAWLER_SANITIZE_URL | true      | No       |

> Note: EnvVar takes precedence than command line flag

## Under the hood

- The crawler will scrape the different recipe links in the root page
- For each link, it starts scraping HTML pages concurrently, and saves the recipe and the link with which it scraped into DB once the scraping is successful.
- The output is a JSON of all recipes. It can be piped to [jq](https://github.com/stedolan/jq) if further processing/querying is needed. e.g

```bash
$ ./recipe-crawler -url $URL  | jq '.[] | .Name'
"Recipe 1"
"Recipe 2"
"Recipe 3"
```

## Clone

```bash
$ git clone https://gitlab.com/aswinkarthik93/karthik_20200209.git
```

## Test

```bash
$ go test -race -v ./...
```

## Build

```bash
$ go build -o recipe-crawler ./main.go
```

## Libraries

1. [BoltDB](https://github.com/etcd-io/bbolt) (persistence)
2. [Assert](https://github.com/stretchr/testify) (only for tests and not in actual code)
3. [HTMLQuery](https://github.com/antchfx/htmlquery) (To parse HTML documents and fetch elements through XPATH queries)
