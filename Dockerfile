# build stage
FROM golang:alpine AS build-env
ADD . /src
RUN cd /src && go build -o recipe-crawler ./main.go

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /src/recipe-crawler /app/recipe-crawler

ENV RECIPE_CRAWLER_DB_PATH "/var/lib/data/data.db"
VOLUME ["/var/lib/data/"]

ENTRYPOINT [ "./recipe-crawler"]
