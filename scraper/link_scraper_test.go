package scraper_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/internal/mock"
	"gitlab.com/aswinkarthik93/karthik/scraper"
	"gitlab.com/aswinkarthik93/karthik/utils"
	"testing"
	"time"
)

func TestLinksScraper_Run(t *testing.T) {
	t.Run("should return error in error channel if root-url fails", func(t *testing.T) {
		client := mock.NewMockClient()
		rootURL := "http://root-url"

		err := client.MockError(rootURL, fmt.Errorf("fetch failed"))
		assert.NoError(t, err)

		linksScraper := scraper.NewLinksScraper(rootURL, client, utils.StringSet{})

		go linksScraper.Run()

		timerCh := time.NewTimer(500 * time.Millisecond).C
		select {
		case err := <-linksScraper.ErrChan():
			assert.EqualError(t, err, "error fetching contents: fetch failed")
			break
		case <-timerCh:
			assert.Fail(t, "no errors were generated")
			break
		}
	})

	t.Run("should return error in error channel if post inside root-url fails", func(t *testing.T) {
		client := mock.NewMockClient()
		rootURL := "http://root-url"
		url1 := "http://root-url/pasta-recipe"
		url2 := "http://root-url/pizza-recipe"
		links := []string{
			url1,
			url2,
		}
		rootHTMLContent, err := mock.LinksHTML(links)
		assert.NoError(t, err)

		err = client.MockResponse(rootURL, rootHTMLContent)
		assert.NoError(t, err)
		err = client.MockError(url1, fmt.Errorf("error fetching sub URL"))
		assert.NoError(t, err)
		err = client.MockResponse(url2, mock.RecipeHTML("Pizza"))
		assert.NoError(t, err)

		linksScraper := scraper.NewLinksScraper(rootURL, client, utils.StringSet{})

		go linksScraper.Run()

		timerCh := time.NewTimer(500 * time.Millisecond).C
		select {
		case err := <-linksScraper.ErrChan():
			assert.EqualError(t, err, "error in fetching URL http://root-url/pasta-recipe: error fetching sub URL")
			break
		case <-timerCh:
			assert.Fail(t, "no errors were generated")
			break
		}
	})

	t.Run("should process links HTML page and parse the recipes from it", func(t *testing.T) {
		client := mock.NewMockClient()
		rootURL := "http://root-url"

		url1 := "http://root-url/pasta-recipe"
		url2 := "http://root-url/pizza-recipe"
		links := []string{
			url1,
			url2,
		}
		pasta := "Pasta"
		pizza := "Pizza"

		recipe1 := domain.ParsedRecipe{
			Recipe: mock.BuildRecipeWithName(pasta),
			Link:   url1,
		}
		recipe2 := domain.ParsedRecipe{
			Recipe: mock.BuildRecipeWithName(pizza),
			Link:   url2,
		}

		rootHTMLContent, err := mock.LinksHTML(links)
		assert.NoError(t, err)

		err = client.MockResponse(rootURL, rootHTMLContent)
		assert.NoError(t, err)
		err = client.MockResponse(url1, mock.RecipeHTML(pasta))
		assert.NoError(t, err)
		err = client.MockResponse(url2, mock.RecipeHTML(pizza))
		assert.NoError(t, err)

		linksScraper := scraper.NewLinksScraper(rootURL, client, utils.StringSet{})

		go linksScraper.Run()

		collectedRecipes := make([]domain.ParsedRecipe, 0, 2)
		timerCh := time.NewTimer(1 * time.Second).C
		for done := false; !done; {
			select {
			case err := <-linksScraper.ErrChan():
				assert.Failf(t, "unexpected error: %v", err.Error())
				break
			case recipe, open := <-linksScraper.RecipeChan():
				if open {
					collectedRecipes = append(collectedRecipes, recipe)
				} else {
					done = true
				}
				break
			case <-timerCh:
				done = true
				assert.Fail(t, "no recipes were generated")
				break
			}
		}

		assert.Len(t, collectedRecipes, 2)
		assert.ElementsMatch(t, []domain.ParsedRecipe{recipe1, recipe2}, collectedRecipes)
	})
}
