package scraper_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/internal/mock"
	"gitlab.com/aswinkarthik93/karthik/scraper"
	"sync"
	"testing"
	"time"
)

func TestRecipeScraper_Run(t *testing.T) {
	t.Run("should send message in error channel if error occurs", func(t *testing.T) {
		client := mock.NewMockClient()

		url := "http://some-url"
		err := client.MockError(url, fmt.Errorf("error in getting URL"))
		assert.NoError(t, err)

		recipeChan := make(chan domain.ParsedRecipe)
		errChan := make(chan error)

		recipeScraper := scraper.NewRecipeScraper(url, client, recipeChan, errChan)
		wg := &sync.WaitGroup{}
		wg.Add(1)

		go recipeScraper.Run(wg)

		timer := time.NewTimer(500 * time.Millisecond).C
		select {
		case err := <-errChan:
			assert.EqualError(t, err, "error in fetching URL http://some-url: error in getting URL")
		case <-timer:
			assert.Fail(t, "no error sent in errChannel")
		}
	})

	t.Run("should send message in error channel if response cannot be parsed", func(t *testing.T) {
		client := mock.NewMockClient()

		url := "http://some-url"
		err := client.MockResponse(url, "wrong content")
		assert.NoError(t, err)

		recipeChan := make(chan domain.ParsedRecipe)
		errChan := make(chan error)

		recipeScraper := scraper.NewRecipeScraper(url, client, recipeChan, errChan)
		wg := &sync.WaitGroup{}
		wg.Add(1)

		go recipeScraper.Run(wg)

		timer := time.NewTimer(500 * time.Millisecond).C
		select {
		case err := <-errChan:
			assert.EqualError(t, err, "error in parsing URL http://some-url: unable to find recipe name from content")
		case <-timer:
			assert.Fail(t, "no error sent in errChannel")
		}
	})

	t.Run("should scrape and send recipes in Recipe channel and in done channel when done", func(t *testing.T) {
		client := mock.NewMockClient()
		url := "http://some-url"
		err := client.MockResponse(url, mock.RecipeHTML("Green Spaghetti"))
		assert.NoError(t, err)
		recipeChan := make(chan domain.ParsedRecipe)
		errChan := make(chan error)

		recipeScraper := scraper.NewRecipeScraper(url, client, recipeChan, errChan)
		wg := &sync.WaitGroup{}
		wg.Add(1)

		go recipeScraper.Run(wg)

		expected := domain.ParsedRecipe{
			Recipe: mock.BuildRecipeWithName("Green Spaghetti"),
			Link:   url,
		}

		timer := time.NewTimer(500 * time.Millisecond).C
		select {
		case recipe := <-recipeChan:
			assert.Equal(t, expected, recipe)
		case err := <-errChan:
			assert.Failf(t, "unexpected error: %v", err.Error())
		case <-timer:
			assert.Fail(t, "no recipe sent in recipe channel")
		}
	})
}
