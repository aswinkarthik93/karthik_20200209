package scraper

import (
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/http"
	"gitlab.com/aswinkarthik93/karthik/parser"
	"gitlab.com/aswinkarthik93/karthik/utils"
	"os"
	"sync"
)

type LinksScraper struct {
	url          string
	client       http.HTMLClient
	errChan      chan error
	recipeChan   chan domain.ParsedRecipe
	visitedLinks utils.StringSet
}

func NewLinksScraper(url string, client http.HTMLClient, visitedLinks utils.StringSet) *LinksScraper {
	return &LinksScraper{
		url:          url,
		client:       client,
		errChan:      make(chan error),
		recipeChan:   make(chan domain.ParsedRecipe),
		visitedLinks: visitedLinks,
	}
}

func (l *LinksScraper) Run() {
	contents, err := l.client.Get(l.url)
	if err != nil {
		l.errChan <- fmt.Errorf("error fetching contents: %v", err)
	}

	links, err := parser.ParseLinksFromHTML(contents)
	if err != nil {
		l.errChan <- fmt.Errorf("error parsing links from URL %s: %v", l.url, err)
	}

	wg := &sync.WaitGroup{}
	for _, link := range links {
		if l.visitedLinks.Contains(link) {
			_, _ = fmt.Fprintf(os.Stderr, "skipping %s as it was already scrapped\n", link)
			continue
		}
		wg.Add(1)
		recipeScraper := NewRecipeScraper(link, l.client, l.recipeChan, l.errChan)

		go recipeScraper.Run(wg)
	}

	wg.Wait()
	close(l.recipeChan)
}

func (l *LinksScraper) ErrChan() chan error {
	return l.errChan
}

func (l *LinksScraper) RecipeChan() chan domain.ParsedRecipe {
	return l.recipeChan
}
