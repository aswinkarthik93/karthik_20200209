package scraper

import (
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"gitlab.com/aswinkarthik93/karthik/http"
	"gitlab.com/aswinkarthik93/karthik/parser"
	"sync"
)

type RecipeScraper struct {
	client     http.HTMLClient
	url        string
	resultChan chan<- domain.ParsedRecipe
	errChan    chan<- error
}

func NewRecipeScraper(
	url string,
	client http.HTMLClient,
	resultChan chan<- domain.ParsedRecipe,
	errChan chan<- error,
) *RecipeScraper {
	return &RecipeScraper{
		client:     client,
		url:        url,
		resultChan: resultChan,
		errChan:    errChan,
	}
}

func (r *RecipeScraper) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	content, err := r.client.Get(r.url)
	if err != nil {
		r.errChan <- fmt.Errorf("error in fetching URL %s: %v", r.url, err)
		return
	}

	recipe, err := parser.ParseRecipeFromHTML(content)
	if err != nil {
		r.errChan <- fmt.Errorf("error in parsing URL %s: %v", r.url, err)
		return
	}

	r.resultChan <- domain.ParsedRecipe{
		Recipe: recipe,
		Link:   r.url,
	}
}
