package http

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewHTMLClient(t *testing.T) {
	client := NewHTTPClient()

	_, ok := client.(*httpClient)
	assert.True(t, ok)
}

func TestHTMLClient_Get(t *testing.T) {
	// This test will fail if there is no network connectivity
	// Unskip this test to check if http Client works
	t.SkipNow()
	client := NewHTTPClient()

	body, err := client.Get("https://google.com")
	assert.NoError(t, err)
	assert.Truef(t, strings.Contains(body, "<title>Google</title>"), "failed to fetch URL")
}
