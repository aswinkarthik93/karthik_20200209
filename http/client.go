package http

import (
	"fmt"
	"io/ioutil"
	netHttp "net/http"
)

// HTMLClient can be used to access webpages
type HTMLClient interface {
	// Get will fetch the given URL and return the body as string
	Get(url string) (string, error)
}

// NewHTTPClient will create a HTMLClient backed by the default
// httpClient. Using this client will use the actual HTTP layer
// to fetch the URL
func NewHTTPClient() HTMLClient {
	return &httpClient{
		c: netHttp.DefaultClient,
	}
}

type httpClient struct {
	c *netHttp.Client
}

func (h *httpClient) Get(url string) (string, error) {
	resp, err := h.c.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("error reading body for URL %s: %v", url, err)
	}

	return string(body), nil
}
