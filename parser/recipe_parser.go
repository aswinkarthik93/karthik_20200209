package parser

import (
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"strings"

	"github.com/antchfx/htmlquery"
	"golang.org/x/net/html"
)

// ParseRecipeFromHTML will read the HTML content and use
// XPath on HTML to parse the recipe details and return a recipe object.
func ParseRecipeFromHTML(htmlContent string) (domain.Recipe, error) {
	doc, err := htmlquery.Parse(strings.NewReader(htmlContent))
	r := domain.Recipe{}
	if err != nil {
		return r, fmt.Errorf("error parsing html content: %v", err)
	}
	name, err := name(doc)
	if err != nil {
		return r, err
	}

	description, err := description(doc)
	if err != nil {
		return r, err
	}

	ingredients, err := ingredients(doc)
	if err != nil {
		return r, err
	}

	steps, err := steps(doc)
	if err != nil {
		return r, err
	}

	categories, err := categories(doc)
	if err != nil {
		return r, err
	}

	tags, err := tags(doc)
	if err != nil {
		return r, err
	}
	return domain.Recipe{
		Name:             name,
		Description:      description,
		Ingredients:      ingredients,
		PreparationSteps: steps,
		Categories:       categories,
		Tags:             tags,
	}, nil
}

func name(doc *html.Node) (string, error) {
	recipeName, err := findOne(doc, "recipe name", `//head/title`)
	if err != nil {
		return "", err
	}

	split := strings.Split(recipeName, "-")
	if len(split) < 2 {
		return strings.TrimSpace(recipeName), nil
	}

	return strings.TrimSpace(split[0]), nil
}

func description(doc *html.Node) (string, error) {
	return findOne(doc, "description", `//*[@id="description"]/following-sibling::p`)
}

func ingredients(doc *html.Node) ([]string, error) {
	return find(doc, "ingredients", `//h3[@id="ingredients"]/following-sibling::ul[1]/li`)
}

func steps(doc *html.Node) ([]string, error) {
	return find(doc, "steps", `//h3[@id="steps"]/following-sibling::ul[1]/li`)
}

func categories(doc *html.Node) ([]string, error) {
	return find(doc, "categories", `//strong[contains(text(),"Categories")]/following-sibling::ul[1]/li/a`)
}

func tags(doc *html.Node) ([]string, error) {
	return find(doc, "tags", `//strong[contains(text(),"Tags")]/following-sibling::a[@class="label label-default"]`)
}

func findOne(doc *html.Node, contentIdentifier, htmlPath string) (string, error) {
	node := htmlquery.FindOne(doc, htmlPath)
	if node == nil {
		return "", fmt.Errorf("unable to find %s from content", contentIdentifier)
	}

	if node.FirstChild == nil {
		return "", fmt.Errorf("unable to find %s from content as value missing", contentIdentifier)
	}

	return node.FirstChild.Data, nil

}

func find(doc *html.Node, contentIdentifier, htmlPath string) ([]string, error) {
	nodes := htmlquery.Find(doc, htmlPath)
	if nodes == nil {
		return nil, fmt.Errorf("unable to find %s from content", contentIdentifier)
	}

	result := make([]string, 0, len(nodes))
	for _, node := range nodes {
		result = append(result, node.FirstChild.Data)
	}

	return result, nil
}
