package parser_test

import (
	"gitlab.com/aswinkarthik93/karthik/internal/mock"
	"gitlab.com/aswinkarthik93/karthik/parser"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseRecipeFromHTML(t *testing.T) {
	t.Run("should parse HTML content and return a Recipe", func(t *testing.T) {
		recipe, err := parser.ParseRecipeFromHTML(mock.RecipeHTML("Green Spaghetti"))

		assert.NoError(t, err)
		assert.Equal(t, mock.BuildRecipeWithName("Green Spaghetti"), recipe)
	})
}
