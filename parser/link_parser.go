package parser

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"strings"
)

// ParseLinksFromHTML will parse the HTML content and use
// XPATH to retrieve all posts link to recipes
func ParseLinksFromHTML(content string) ([]string, error) {
	doc, err := htmlquery.Parse(strings.NewReader(content))
	if err != nil {
		return nil, fmt.Errorf("error parsing links from content: %v", err)
	}

	links := make([]string, 0)

	nodes := htmlquery.Find(doc, `//ul/li/h5/a`)
	for _, node := range nodes {
		for _, attr := range node.Attr {
			if attr.Key == "href" && attr.Val != "" {
				links = append(links, attr.Val)
			}
		}
	}

	if len(links) == 0 {
		return nil, fmt.Errorf("no links found for further scraping")
	}

	return links, nil
}
