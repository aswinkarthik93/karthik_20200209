package parser_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/aswinkarthik93/karthik/internal/mock"
	"gitlab.com/aswinkarthik93/karthik/parser"
	"testing"
)

func TestParseLinksFromHTML(t *testing.T) {
	t.Run("should parse links from HTML content", func(t *testing.T) {

		expectedLinks := []string{
			"http://some-url/post/green-spaghetti/",
			"http://some-url/post/nilla-wafer/",
			"http://some-url/post/pork-steaks/",
			"http://some-url/post/red-berry-tart/",
			"http://some-url/post/roasted-okra/",
			"http://some-url/post/smashed-carrots/",
			"http://some-url/post/broccoli-beer-cheese-soup/",
			"http://some-url/post/cauliflower-cacciatore/",
			"http://some-url/post/downtown-marinade/",
			"http://some-url/post/crispy-carrots/",
			"http://some-url/post/chile-stew/",
			"http://some-url/post/crockpot-chicken/",
		}

		html, err := mock.LinksHTML(expectedLinks)
		assert.NoError(t, err)

		links, err := parser.ParseLinksFromHTML(html)
		assert.NoError(t, err)
		assert.Equal(t, expectedLinks, links)
	})
}
