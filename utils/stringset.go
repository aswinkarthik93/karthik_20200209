package utils

type StringSet map[string]struct{}

func NewStringSet(items []string) StringSet {
	s := make(StringSet)

	for _, l := range items {
		s[l] = struct{}{}
	}

	return s
}

func (s StringSet) Contains(item string) bool {
	_, ok := s[item]
	return ok
}
