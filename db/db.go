package db

import (
	"encoding/json"
	"fmt"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"sort"

	bolt "go.etcd.io/bbolt"
)

// DB interface can be used to interact with persistence layer
type DB interface {
	// AddRecipeWithLink will add the given recipe to DB.
	// Recipe's name is used to uniquely identify a recipe.
	// Returns error if a recipe with name already exists
	AddRecipeWithLink(recipe domain.Recipe, link domain.Link) error

	// ListRecipes with list all saved recipes and return it as a slice
	// Returns an empty slice if there are no recipes.
	ListRecipes() ([]domain.Recipe, error)

	// ListLinks fetches all links from DB and returns it as slice of strings
	// Returns an empty slice if there are no links.
	ListLinks() ([]string, error)
}

// NewBoltDB is a constructutor for creating a DB.
// It creates a db in provided filepath and returns error if
// failing to do so. Bolt options can be provided in case
// to use a temp file implementation.
func NewBoltDB(filepath string, options *bolt.Options) (DB, error) {
	b, err := bolt.Open(filepath, 0600, options)
	if err != nil {
		return nil, fmt.Errorf("error creating bolt DB: %v", err)
	}
	return &boltDB{b}, nil
}

const (
	// recipeBucket is the bucket to hold data on recipes
	recipeBucket string = "recipes"
	// linksBucket is the bucket to hold data on links
	linksBucket string = "links"
)

type boltDB struct {
	db *bolt.DB
}

// AddRecipeWithLink implementation with BoltDB backend.
// Adds it to the "recipes" bucket
func (b *boltDB) AddRecipeWithLink(recipe domain.Recipe, link domain.Link) error {
	return b.db.Update(func(tx *bolt.Tx) error {
		if err := b.addRecipe(tx, recipe); err != nil {
			return fmt.Errorf("error adding recipe: %v", err)
		}

		if err := b.addLink(tx, link); err != nil {
			return fmt.Errorf("error adding link: %v", err)
		}

		return nil
	})
}

func (b *boltDB) addRecipe(tx *bolt.Tx, recipe domain.Recipe) error {
	bucket, err := tx.CreateBucketIfNotExists([]byte(recipeBucket))
	if err != nil || b == nil {
		return fmt.Errorf("error creating \"%s\" bucket: %v", recipeBucket, err)
	}

	if existingRecipe := bucket.Get([]byte(recipe.Name)); existingRecipe != nil {
		return fmt.Errorf("recipe with name \"%s\" already exists", recipe.Name)
	}

	// This returns an error only if the Tx is closed or not writeable.
	// That can't happen in an Update() call so I ignore the error check.
	id, _ := bucket.NextSequence()
	recipe.ID = id

	data, err := json.Marshal(recipe)
	if err != nil {
		return fmt.Errorf("error serializing recipe \"%s\": %v", recipe.Name, err)
	}

	if err := bucket.Put([]byte(recipe.Name), data); err != nil {
		return fmt.Errorf("error inserting recipe \"%s\": %v", recipe.Name, err)
	}

	return nil
}

func (b *boltDB) addLink(tx *bolt.Tx, link domain.Link) error {
	bucket, err := tx.CreateBucketIfNotExists([]byte(linksBucket))
	if err != nil || b == nil {
		return fmt.Errorf("error creating \"%s\" bucket: %v", linksBucket, err)
	}

	if existingLink := bucket.Get([]byte(link.URL)); existingLink != nil {
		return fmt.Errorf("link already exists")
	}

	data, err := json.Marshal(link)
	if err != nil {
		return fmt.Errorf("error serializing link \"%s\": %v", link.URL, err)
	}

	if err := bucket.Put([]byte(link.URL), data); err != nil {
		return fmt.Errorf("error inserting link: \"%s\"", link.URL)
	}

	return nil
}

// ListLinks implementation with BoltDB backend.
// Lists all all keys of "links" bucket
func (b *boltDB) ListLinks() ([]string, error) {
	links := make([]string, 0)
	err := b.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(linksBucket))
		if bucket == nil {
			return nil
		}

		c := bucket.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			links = append(links, string(k))
		}

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("error in listing links: %v", err)
	}

	return links, nil
}

// ListRecipes implementation with BoltDB backend
// Lists all values of "recipes" bucket. Sorts the result by ID.
func (b *boltDB) ListRecipes() ([]domain.Recipe, error) {
	recipes := make([]domain.Recipe, 0)
	err := b.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(recipeBucket))
		if bucket == nil {
			return nil
		}

		c := bucket.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var recipe domain.Recipe
			err := json.Unmarshal(v, &recipe)
			if err != nil {
				return fmt.Errorf("error unmarshal'ing recipe: %v", err)
			}
			recipes = append(recipes, recipe)
		}

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf("error in listing recipes: %v", err)
	}

	sort.SliceStable(recipes, func(i, j int) bool {
		return recipes[i].ID < recipes[j].ID
	})

	return recipes, nil
}
