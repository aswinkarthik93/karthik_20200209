package db

import (
	"encoding/json"
	"gitlab.com/aswinkarthik93/karthik/domain"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	bolt "go.etcd.io/bbolt"
)

type TestBoltDB struct {
	file *os.File
}

func NewTestBoltDB(t *testing.T) *TestBoltDB {
	file, err := ioutil.TempFile(os.TempDir(), "boltdb")
	if err != nil {
		assert.NoError(t, err)
		t.FailNow()
		return nil
	}

	return &TestBoltDB{file}
}

func (t *TestBoltDB) CleanUp() {
	os.Remove(t.file.Name())
	t.file.Close()
}

func (t *TestBoltDB) BoltOptions() *bolt.Options {
	return &bolt.Options{
		OpenFile: func(string, int, os.FileMode) (*os.File, error) {
			return t.file, nil
		},
	}
}

func TestNewBoltDB(t *testing.T) {
	t.Run("should create new bolt DB", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		boltDb, err := NewBoltDB("", options)

		assert.NoErrorf(t, err, "error creating boltdb")
		assert.NotNil(t, boltDb)
	})
}

func TestDB_AddRecipeWithLink(t *testing.T) {
	t.Run("should add recipe to DB with ID", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		recipe := domain.Recipe{Name: "Pizza", Description: "Describes a pizza"}
		link := domain.Link{URL: "http://some-url", RecipeName: "Pizza"}

		err = db.AddRecipeWithLink(recipe, link)

		assert.NoErrorf(t, err, "error adding recipe")

		actualDb := db.(*boltDB).db
		actualDb.View(func(tx *bolt.Tx) error {
			recipesBucket := tx.Bucket([]byte("recipes"))
			if !assert.NotNilf(t, recipesBucket, "bucket \"recipes\" expected to be not nil") {
				return nil
			}

			linksBucket := tx.Bucket([]byte("links"))
			if !assert.NotNilf(t, linksBucket, "bucket \"links\" expected to be not nil") {
				return nil
			}

			expectedPizza := recipe
			expectedPizza.ID = 1
			pizzaInDB := domain.Recipe{}
			if data := recipesBucket.Get([]byte("Pizza")); data != nil {
				assert.NoError(t, json.Unmarshal(data, &pizzaInDB))
			}
			assert.Equal(t, expectedPizza, pizzaInDB)

			var linkInDB domain.Link
			if data := linksBucket.Get([]byte(link.URL)); data != nil {
				assert.NoError(t, json.Unmarshal(data, &linkInDB))
			}
			assert.Equal(t, link, linkInDB)

			return nil
		})
	})

	t.Run("should fail if recipe to be added already exists", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		recipe := domain.Recipe{Name: "Pizza", Description: "Describes a pizza"}
		link := domain.Link{URL: "http://some-url", RecipeName: "Pizza"}

		err = db.AddRecipeWithLink(recipe, link)
		assert.NoErrorf(t, err, "error adding recipe")

		err = db.AddRecipeWithLink(recipe, link)
		assert.EqualError(t, err, `error adding recipe: recipe with name "Pizza" already exists`)
	})
}

func TestDB_ListLinks(t *testing.T) {
	t.Run("should return empty list if no links present", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		links, err := db.ListLinks()
		assert.NoError(t, err)
		assert.Equal(t, []string{}, links)
	})

	t.Run("should list URLs saved in links bucket", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		recipe1 := domain.Recipe{Name: "Recipe1", Description: "Describes a recipe1"}
		link1 := domain.Link{URL: "http://url-1", RecipeName: "Recipe1"}
		recipe2 := domain.Recipe{Name: "Recipe2", Description: "Describes a recipe2"}
		link2 := domain.Link{URL: "http://url-2", RecipeName: "Recipe2"}
		recipe3 := domain.Recipe{Name: "Recipe3", Description: "Describes a recipe3"}
		link3 := domain.Link{URL: "http://url-3", RecipeName: "Recipe3"}

		err = db.AddRecipeWithLink(recipe1, link1)
		assert.NoErrorf(t, err, "error adding link")
		err = db.AddRecipeWithLink(recipe2, link2)
		assert.NoErrorf(t, err, "error adding link")
		err = db.AddRecipeWithLink(recipe3, link3)
		assert.NoErrorf(t, err, "error adding link")

		links, err := db.ListLinks()

		assert.NoError(t, err)
		assert.Equal(t, []string{"http://url-1", "http://url-2", "http://url-3"}, links)
	})
}

func TestDB_ListRecipes(t *testing.T) {
	t.Run("should return empty list if no recipes present", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		links, err := db.ListRecipes()
		assert.NoError(t, err)
		assert.Equal(t, []domain.Recipe{}, links)
	})

	t.Run("should list recipes saved in recipes bucket", func(t *testing.T) {
		testDb := NewTestBoltDB(t)
		defer testDb.CleanUp()
		options := testDb.BoltOptions()
		db, err := NewBoltDB("", options)
		assert.NoErrorf(t, err, "error creating boltdb")

		recipe1 := domain.Recipe{ID: 1, Name: "Recipe1", Description: "Describes a recipe1"}
		link1 := domain.Link{URL: "http://url-1", RecipeName: "Recipe1"}
		recipe2 := domain.Recipe{ID: 2, Name: "Recipe2", Description: "Describes a recipe2"}
		link2 := domain.Link{URL: "http://url-2", RecipeName: "Recipe2"}
		recipe3 := domain.Recipe{ID: 3, Name: "Recipe3", Description: "Describes a recipe3"}
		link3 := domain.Link{URL: "http://url-3", RecipeName: "Recipe3"}

		err = db.AddRecipeWithLink(recipe1, link1)
		assert.NoErrorf(t, err, "error adding link")
		err = db.AddRecipeWithLink(recipe2, link2)
		assert.NoErrorf(t, err, "error adding link")
		err = db.AddRecipeWithLink(recipe3, link3)
		assert.NoErrorf(t, err, "error adding link")

		recipes, err := db.ListRecipes()
		expectedRecipes := []domain.Recipe{recipe1, recipe2, recipe3}

		assert.NoError(t, err)
		assert.Equal(t, expectedRecipes, recipes)
	})
}
