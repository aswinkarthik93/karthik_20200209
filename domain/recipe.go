package domain

// Recipe represents the recipe domain
type Recipe struct {
	ID               uint64
	Name             string
	Description      string
	Ingredients      []string
	PreparationSteps []string
	Categories       []string
	Tags             []string
}

// ParsedRecipe represents a result scraped from a web-page
type ParsedRecipe struct {
	Recipe Recipe
	Link   string
}

// Link is a domain model that links a URL with the recipe name
type Link struct {
	URL        string
	RecipeName string
}
